package hu.bme.aut.android.androidwallet

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.forEach
import com.google.android.material.snackbar.Snackbar
import hu.bme.aut.android.androidwallet.databinding.ActivityMainBinding
import hu.bme.aut.android.androidwallet.databinding.SalaryRowBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var rowBinding: SalaryRowBinding

    private var sum: Double = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)



        binding.saveButton.setOnClickListener {
            if (binding.salaryName.text.toString().isEmpty() || binding.salaryAmount.text.toString()
                    .isEmpty()
            ) {
                currentFocus?.let {
                    Snackbar.make(it, R.string.warn_message, Snackbar.LENGTH_LONG).show()
                }
                return@setOnClickListener
            }
            rowBinding = SalaryRowBinding.inflate(layoutInflater)

            rowBinding.salaryDirectionIcon.setImageResource(if (binding.expenseOrIncome.isChecked) R.drawable.expense else R.drawable.income)
            rowBinding.rowSalaryName.text = binding.salaryName.text.toString()
            rowBinding.rowSalaryAmount.text = binding.salaryAmount.text.toString()

            if (binding.expenseOrIncome.isChecked) {
                sum -= binding.salaryAmount.text.toString().toDouble()
            }
            else {
                sum += binding.salaryAmount.text.toString().toDouble()
            }
            binding.sum.text = "Sum: $sum"

            binding.listOfRows.addView(rowBinding.root)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.delete_all -> {
                binding.listOfRows.removeAllViews()
                sum = 0.0
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}